//
//  TransferView.swift
//  TonChan
//
//  Created by Ivan Hector Aquino Apaza on 19/5/21.
//

import SwiftUI

struct TransferView: View {
    @ObservedObject var viewRouter: ViewRouter
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct TransferView_Previews: PreviewProvider {
    static var previews: some View {
        TransferView(viewRouter: ViewRouter())
    }
}
